package com.example.stane.livedatademo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import static java.util.Collections.addAll;

/**
 * Created by Stane on 18.01.2018.
 */

public class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private Callback callback;

    public Adapter(Callback callback) {
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(callback != null) {
            callback.setItem(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        if(callback != null) {
            return callback.getItemCount();
        }
        return 0;
    }

    public interface Callback {
        void setItem(ViewHolder holder, int position);
        int getItemCount();
    }

    public void setFilter(List<Model> items){
        addAll(items);
        notifyDataSetChanged();
    }
}
