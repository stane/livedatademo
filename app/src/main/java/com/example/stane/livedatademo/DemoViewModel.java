package com.example.stane.livedatademo;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Stane on 18.01.2018.
 */

public class DemoViewModel extends ViewModel {

    // Create a LiveData with a String
    private MutableLiveData<List<Model>> model;

    public MutableLiveData<List<Model>> getCurrentModel() {
        if (model == null) {
            model = new MutableLiveData<>();
            model.setValue(loadData());
        }
        return model;
    }

    private List<Model> loadData() {
        List<Model> modelList = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<10; i++) {
            Model model = new Model();
            model.setSecurityName("Security Name " + i);
            model.setPrice(random.nextInt(100)+1);
            modelList.add(model);
        }
        return modelList;
    }

}
