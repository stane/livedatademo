package com.example.stane.livedatademo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Stane on 18.01.2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    private TextView securityName;
    private TextView price;

    public ViewHolder(View itemView) {
        super(itemView);
        securityName = itemView.findViewById(R.id.security_name);
        price = itemView.findViewById(R.id.price);
    }

    public void setSecurityName(String securityName) {
        if(this.securityName != null && securityName != null) {
            this.securityName.setText(securityName);
        }
    }

    public void setPrice(String price) {
        if(this.price != null && price != null) {
            this.price.setText(price);
        }
    }

}

