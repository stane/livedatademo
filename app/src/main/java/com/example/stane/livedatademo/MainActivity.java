package com.example.stane.livedatademo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements Adapter.Callback {

    private DemoViewModel demoModel;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        demoModel = ViewModelProviders.of(this).get(DemoViewModel.class);

        // Create the observer which updates the UI.
        final Observer<List<Model>> modelObserver = new Observer<List<Model>>() {
            @Override
            public void onChanged(@Nullable final List<Model> model) {
                // Update the UI
                adapter.notifyDataSetChanged();

            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        demoModel.getCurrentModel().observe(this, modelObserver);


        adapter = new Adapter(this);
        RecyclerView recyclerView = findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        final Random random = new Random();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int randomPosition = random.nextInt(demoModel.getCurrentModel().getValue().size());
                double randomPrice = random.nextDouble();
                List<Model> model = demoModel.getCurrentModel().getValue();
                model.get(randomPosition).setPrice(randomPrice);
                //update model
                demoModel.getCurrentModel().setValue(model);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setItem(ViewHolder holder, int position) {
        holder.setSecurityName(demoModel.getCurrentModel().getValue().get(position).getSecurityName());
        holder.setPrice(String.valueOf(demoModel.getCurrentModel().getValue().get(position).getPrice()));
    }

    @Override
    public int getItemCount() {
        return demoModel.getCurrentModel().getValue().size();
    }

}
