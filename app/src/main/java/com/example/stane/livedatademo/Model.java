package com.example.stane.livedatademo;

/**
 * Created by Stane on 18.01.2018.
 */

public class Model {

    private String securityName;
    private double price;

    public String getSecurityName() {
        return securityName;
    }

    public void setSecurityName(String securityName) {
        this.securityName = securityName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
